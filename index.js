// console.log("Hello")

function displayMsgToSelf(){
	console.log("Don't text him back");
};

// invoke 10 times
// send your console output screenshot in hangouts

displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();

// While Loop
	/*
		Syntax:
		while(condition){
			statement
		};


	*/

function displayMsgToSelf(){
	console.log("Don't text him back");
};

let count = 20;

while(count !== 0){
	displayMsgToSelf();
	count--;
};

/*
Mini-Activity
	The following while loop should dispay the numbers from 1-5 (in order);
*/

let x = 1

while(x <= 5){
	console.log(x);
	x++;
};

//Do-while Loop
/*
	Syntax:
	do {
		statement
	} while (condition)

*/

let doWhileCounter = 1;
do {
	console.log(doWhileCounter)
} while(doWhileCounter === 0)

let number = Number(prompt("Give me a number"));
do {
	console.log("Do While: " + number)
	number += 1
} while(number < 10)

// Mini-activity

// PEDENG ITO
let doWhile = 1;
do {
	console.log("Count: " + doWhile)
	doWhile++
} while(doWhile <= 20)

// PEDE DIN ITO
let count1 = 1;
do {
	console.log("Do While: " + count1)
	count1 += 1
} while(count1 < 21)

// For Loop
	/*
		for(initialization; condition; finalExpression(increment/decrement);){
			
			statement
		}

	*/

for(let count = 0; count <= 10; count++){ 
	console.log("For Loop Count: " + count)
}


// index starts at 0 - [i]
let name = "Alex";

// A === [O]
// l === [1]
// e === [2]
// x === [3]

// Strings are special compared to other data types in that it has access to functions and other pieces of information another primitive data type might not have
// console.log(myString.length);

// Will create a loop that will print out the individual letters of the name variable


for(let i = 0; i < name.length; i++){

// If the character of your name is a vowel letter, instead of displaying the character, display number "3"
// The ".toLowerCase()" function/method will change the current letter being evaluated in a loop to a lowercase letter ensuring that the letters provided in the expressions below will match

	if(
		name[i].toLowerCase() == "a" ||
		name[i].toLowerCase() == "e" ||
		name[i].toLowerCase() == "i" ||
		name[i].toLowerCase() == "o" ||
		name[i].toLowerCase() == "u" 
		){

// If the letter in the name is a vowel, it will print the number 3	
		console.log(3);
	} else {

// Print in the console all non-vowel characters in the name		
		console.log(name[i])
	}
};

// Continue and Break

for(let count = 0; count <= 20; count++){

	if(count % 2 === 0){

		continue;
	}
	console.log("Continue and Break: " + count);

	if(count > 10){
		break;
	};
};

let myName = "alexandro"

for(let i = 0; i < myName.length; i++){

	console.log(myName[i]);

	if(myName[i].toLowerCase() == "a"){

		console.log("Continue to next iteration")

		continue; // ignore all the remaining codes below once condition met, then will continue to the next iteration
	};

	if(myName[i].toLowerCase() === "d"){

		break; // get out of the loops once condition met
	};

};